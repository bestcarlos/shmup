﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovePatternScript : MonoBehaviour {

    public enum Pattern { straight, sinus, rounded };
    public Pattern pattern;
    public float speed;
    Rigidbody rb;
    public bool destroyOnComplete;
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        switch (pattern)
        {

            case Pattern.straight:

                rb.DOMoveX(-40f, 10f / speed, false).SetEase(Ease.Linear).OnComplete(AutoDestroy);
                break;

            case Pattern.sinus:

                rb.DOMoveX(-40f, 10f / speed, false).SetEase(Ease.Linear).OnComplete(AutoDestroy);
                rb.DOMoveY(transform.position.y + 2f, 1f / speed, false).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
                break;

            case Pattern.rounded:

                rb.DOMoveX(-20f, 10f / speed, false).SetEase(Ease.Linear).OnComplete(AutoDestroy);
                break;
        }

    }
    void AutoDestroy()
    {
        if (destroyOnComplete)
            Destroy(this.gameObject);
    }



}

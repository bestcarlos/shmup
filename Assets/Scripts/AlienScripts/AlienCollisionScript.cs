﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienCollisionScript : MonoBehaviour {

    // Use this for initialization
    public GameObject alienCol;
    private void OnCollisionEnter(Collision passingEntity)
    {
        if (passingEntity.gameObject.tag == "Alien")
        {
            Physics.IgnoreCollision(passingEntity.collider, alienCol.GetComponent<Collider>());
        }
    }
}

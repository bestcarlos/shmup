﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienTriggerScript : MonoBehaviour {

    public int quantity;
    public bool isDestroyed;
    bool crash = false;


    void OnTriggerEnter(Collider objetTouche)
    {
        if (objetTouche.tag == "Player")
         {
            isDestroyed = true;
            objetTouche.transform.parent.GetComponent<DamageScript>().currentLife = 0;
         }
        else {
            objetTouche.SendMessageUpwards("Damage", quantity, SendMessageOptions.DontRequireReceiver);

            
        }
        if (isDestroyed)
            Destroy(this.gameObject);

    }
}
